package cat.itb.exercici;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Virus {




    Image virus;
    private final Image deadVirus = new ImageIcon("virusdead.png").getImage();

    private int x = 0;
    private int y = 0;
    private double dx = 1;
    private double dy = 1;

    private JPanel elPanelComu; //el panell on reboten les pilotes

    public void setElPanelComu(JPanel p) {
        elPanelComu = p;
    }

    public Image getVirus() {
        return virus;
    }

    public void moveVirus(Rectangle2D limits) {
        double width = limits.getWidth();
        double height = limits.getHeight();
        x += dx;
        y += dy;
        int radi = 15;
        if (x + radi / 2 > width || x + radi / 2 < 0) dx = -dx;
        if (y + radi / 2 > height || y + radi / 2 < 0) dy = -dy;

        virus = new ImageIcon("virus.png").getImage();
    }

    public void paintVirus(Graphics g, boolean on) {
        Graphics2D g2 = (Graphics2D) g;

        if (on) g2.drawImage(virus, x, y, null);
        else g2.drawImage(deadVirus, x, y, null);

    }

    /**
     * Getter de la posició del virus
     *
     * @return posició x e y del virus
     */


    public Object getX() {
        return null;
    }

    public Object getY() {
        return null;
    }


}

class ThreadVirus implements Runnable {

    private Virus p;
    private PanelVirus panel;
    private int virusSpeed = (int) Math.floor(Math.random() * 6 + 1);

    public ThreadVirus(Virus p, PanelVirus panel) {
        this.p = p;
        this.panel = panel;
    }

    public Virus getP() {
        return p;
    }

    public PanelVirus getPanel() {
        return panel;
    }

    public boolean life(boolean alive) {
        if(alive) {
            return true;
        }else{
            virusSpeed = 0;
            return false;
        }
    }

    @Override
    public void run() {

        for (; ;) {
            p.moveVirus(panel.getBounds());
            try {
                Thread.sleep(virusSpeed);
            } catch (Exception e) {
                Thread.currentThread().interrupt();
            }
            panel.repaint();
        }
    }
}