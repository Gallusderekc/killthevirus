package cat.itb.exercici;

import javax.swing.*;

public class KillTheVirus {
    /**
     * classe equivalent al Main de Pilotes
     */

    public static void main(String[] args) {
        JFrame joc=new KillTheVirusGUI();
        joc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        joc.setVisible(true);
    }
}
