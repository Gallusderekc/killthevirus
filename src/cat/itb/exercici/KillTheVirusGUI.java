package cat.itb.exercici;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Ramses Sanchez Barragan
 * @version 0.1
 * @description
 * @since 31/10/2021
 */
public class KillTheVirusGUI extends JFrame {

    private final PanelVirus panelVirus = new PanelVirus();


    /**
     * classe per crear la interficie grafica del programa
     * te dos buttons per iniciar i sortir del joc
     */
    public KillTheVirusGUI() {
        setBounds(600, 300, 400, 350);
        setTitle("Kill The Virus");
        add(panelVirus, BorderLayout.CENTER);
        JPanel botons = new JPanel();
        JButton boto1 = new JButton("Kill The COVID");
        JButton boto2 = new JButton("Exit");
        botons.add(boto1);
        botons.add(boto2);
        boto1.addActionListener(new ClickButtonCovid());
        boto2.addActionListener(new ClickExit());
        add(botons, BorderLayout.SOUTH);
    }


    class ClickButtonCovid implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent evt) {


            Virus v = new Virus();
            panelVirus.add(panelVirus);
            Runnable r = new ThreadVirus(v, panelVirus);
            Thread t = new Thread(r);
            t.start();

        }
    }

    class ClickExit implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent evt) {
            System.exit(0);
        }
    }
}
