package cat.itb.exercici;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class PanelVirus extends JPanel implements MouseListener {
    private ArrayList<ThreadVirus> virus = new ArrayList<>();

    public PanelVirus(){
        setBackground(Color.white);
        addMouseListener(this);
    }



    public void paint(Graphics graphics) {
        super.paint(graphics);
        Graphics2D graphics2D = (Graphics2D) graphics;
        for (ThreadVirus screen : virus) {

            screen.getP().paintVirus(graphics2D, screen.life(true));
        }
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void mouseClicked(MouseEvent clicar) {
        virus.add(v);
    }

    @Override
    public void mousePressed(MouseEvent pressed) {

        /**
         * Fent aquest sistema em permet parar el virus al donar-me les coordenades de la seva posició
         */
        int xEsq = pressed.getX() - 60;
        int xDre = pressed.getX() + 60;
        int yAmu = pressed.getY() + 60;
        int yAva = pressed.getY() - 60;

        for (ThreadVirus screen : virus) {

            /**
             * Comparo la posicó del virus amb la del mousePressed, si coincideixen  canvien en virus i es para el threah
             */
            int virusX = (int) screen.getP().getX();
            int virusY = (int) screen.getP().getY();

            if ((virusX >= xEsq && virusX <= xDre) && (virusY >= yAva && virusY <= yAmu)) {
                screen.life(false);
            }
        }
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
    }

    public void add(ThreadVirus p) {
        virus.add(p);
    }
}
